# met.3d.installer_script

The script `install_met3d.sh` will help you install the 3D visualization software [Met.3D](https://gitlab.com/wxmetvis/met.3d) using conda.

To use the script, clone this repository and execute the `install_met3d.sh` script.
When you don't have conda installed, the script will install it for you, but will require you to
restart the terminal before proceeding with the Met.3D installation.
Afterwards it will guide you through the installation process.

When the installation is finished, Met.3D will open once.
If you want to start Met.3D afterwards, you have to go to the install directory you choose while installing it,
called the base directory. Inside you will find a build folder. Inside the build folder open a terminal and start Met.3D with the following command

```
MET3D_HOME=BaseDirectory/met.3d MET3D_BASE=BaseDirectory ./Met3D
```

and replace BaseDirectory with the path to the Met.3D installation directory.