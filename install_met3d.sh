#!/usr/bin/bash -l
ScriptLoc=$(readlink -f "$0")
if ! command -v conda &> /dev/null
then
echo "Conda is not installed"
echo "When prompted to initialize conda, type yes."
read -p "Press [Enter] to install conda..."
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    bash Miniconda3-latest-Linux-x86_64.sh
    rm ./Miniconda3-latest-Linux-x86_64.sh
    echo "Conda is now installed. Please manually restart your terminal and relaunch the script to initialize conda correctly."
    read -p "Press [Enter] to exit..."
    exit 0
fi
echo "Conda is already installed."
read -p "Conda environment name? (met3d): " condaenv
eval "$(conda shell.bash hook)"
condaenv=${condaenv:-"met3d"}

echo "creating conda environment $conadenv"
conda create -n $condaenv
conda activate $condaenv

echo "Installing dependencies in conda environment $condaenv"

conda install -c conda-forge cxx-compiler fortran-compiler make cmake pkg-config gdb glew log4cplus libgdal eccodes freetype gsl proj qt git mesa-libgl-devel-cos7-x86_64 mesa-dri-drivers-cos7-aarch64 libxau-devel-cos7-aarch64 libselinux-devel-cos7-aarch64 libxdamage-devel-cos7-aarch64 libxxf86vm-devel-cos7-aarch64 libxext-devel-cos7-aarch64 xorg-libxfixes xorg-libxau

echo "Installing third party dependencies..."

read -p "Path to the met3d base directory? ($HOME/met.3d-base): " metbasei

metbasei=${metbasei:-"$HOME/met.3d-base"}

# Create a "met.3d-base" directory in your home directory, with sub-dirs "local" and "third-party".
cd ~
mkdir -p $metbasei
if [ $? -ne 0 ]
then
    echo "Could not create folder $metbasei"
    exit 1
fi
metbase=$(eval "realpath $metbasei")
cd $metbase
mkdir local
mkdir third-party

# Checkout and install glfx.
cd $metbase/third-party/
git clone https://github.com/maizensh/glfx.git
cd glfx
cmake -DCMAKE_INSTALL_PREFIX:PATH=$metbase/local CMakeLists.txt
make -j 12
if [ $? -ne 0 ]
then
    echo "Could not compile glfx. Please compile manually."
    exit 1
fi
make install

# Download and install QCustomPlot (download packages from website):
cd $metbase/third-party/
wget https://www.qcustomplot.com/release/2.1.0fixed/QCustomPlot.tar.gz
wget https://www.qcustomplot.com/release/2.1.0fixed/QCustomPlot-sharedlib.tar.gz
tar xvfz QCustomPlot.tar.gz
tar xvfz QCustomPlot-sharedlib.tar.gz
mv qcustomplot-sharedlib/ qcustomplot/

cd qcustomplot/qcustomplot-sharedlib/sharedlib-compilation/
qmake
make -j 12
if [ $? -ne 0 ]
then
    echo "Could not compile qcustomplot. Please compile manually."
    echo "Maybe some paths in the makefile are incorrect. This happens when qmake is also installed outside of conda. Fix manually."
    exit 1
fi
cp libqcustomplot* $metbase/local/lib/
cd ../..
cp qcustomplot.h $metbase/local/include/

# NetCDF4 C++ API is not available in latest version from conda.
cd $metbase/third-party/
wget https://downloads.unidata.ucar.edu/netcdf-cxx/4.3.1/netcdf-cxx4-4.3.1.tar.gz
tar xf netcdf-cxx4-4.3.1.tar.gz
cd netcdf-cxx4-4.3.1
# !! Change <your user> to the correct path in the following command:
./configure --prefix=$metbase/local
make -j 12
if [ $? -ne 0 ]
then
    echo "Could not compile netcdf4. Please compile manually."
    exit 1
fi
make install

# section C), download remaining third-party dependencies
cd $metbase/third-party
git clone https://github.com/qtproject/qt-solutions.git

wget http://ftp.gnu.org/gnu/freefont/freefont-ttf-20120503.zip
unzip freefont-ttf-20120503.zip

mkdir naturalearth
cd naturalearth
wget https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/50m/physical/ne_50m_coastline.zip
unzip ne_50m_coastline.zip
wget https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/50m/cultural/ne_50m_admin_0_boundary_lines_land.zip
unzip ne_50m_admin_0_boundary_lines_land.zip
wget https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/50m/raster/HYP_50M_SR_W.zip
unzip HYP_50M_SR_W.zip

echo "Installing Met.3D..."

cd $metbase

echo "If you use a fork of Met.3D enter its repository here. Otherwise press enter."
read -p "Default (https://gitlab.com/wxmetvis/met.3d.git): " metgit

metgit=${metgit:-"https://gitlab.com/wxmetvis/met.3d.git"}

echo "Cloning git repository $metgit into $metbase/met.3d"
git clone $metgit met.3d
if [ $? -eq 0 ]
then
    mkdir build && cd build
    cmake -DCMAKE_BUILD_TYPE=RELEASE -DCMAKE_PREFIX_PATH=$metbase/local ../met.3d
    make -j 12
    if [ $? -eq 0 ]
    then
        cd ~/met.3d-base/build/
        MET3D_HOME=$metbase/met.3d MET3D_BASE=$metbase ./Met3D
    else
       echo "Could not compile Met3D..."
       exit 1
    fi
else
    echo "Cant clone repository $metgit, aborting installation..."
    cd ~
    echo "Do you want to cleanup the failed installation?"
    select yn in "Yes" "No"; do
        if [[ "$yn" =~ "No" ]]
        then
        rm -r $metbase
        fi
        break
    done
    exit 1
fi

echo "Met3D successfully installed to $metbase/met.3d."
echo "Met.3D base folder: $metbase"
echo "Conda environment name: $condaenv"
echo "Met.3D repository: $metgit"
